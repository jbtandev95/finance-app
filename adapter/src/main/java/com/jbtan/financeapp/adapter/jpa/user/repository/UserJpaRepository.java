package com.jbtan.financeapp.adapter.jpa.user.repository;

import com.jbtan.financeapp.adapter.jpa.user.entity.UserJpaEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserJpaRepository extends JpaRepository<UserJpaEntity, Long> {

    @Query("select user.id " +
        "from UserJpaEntity user " +
        "where user.login = :login " +
        "and user.activated = true ")
    Optional<Long> findIfExistingLoginExists(@Param("login") String login);

    @Query("select user.id " +
        "from UserJpaEntity user " +
        "where user.email = :email " +
        "and user.activated = true ")
    Optional<Long> findIfExistingEmailExists(@Param("email") String email);

    @Query("select user " +
        "from UserJpaEntity user " +
        "where user.login = :login " +
        "and user.activated = true ")
    Optional<UserJpaEntity> findUserByLogin(@Param("login") String login);
}
