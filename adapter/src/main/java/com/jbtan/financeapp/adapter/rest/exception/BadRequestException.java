package com.jbtan.financeapp.adapter.rest.exception;

import org.springframework.security.core.parameters.P;

public class BadRequestException extends RuntimeException {

    private String message;

    public BadRequestException(String message) {
        super(message);
    }
}
