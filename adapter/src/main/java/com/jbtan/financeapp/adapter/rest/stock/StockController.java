package com.jbtan.financeapp.adapter.rest.stock;

import com.jbtan.financeapp.domain.port.in.stock.StockServicePort;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
@Slf4j
@Tag(name = "Stocks", description = "Stocks Operations")
public class StockController {

    private final StockServicePort stockServicePort;

    @GetMapping("/stocks")
    public ResponseEntity<?> findByTickerSymbol(@RequestParam String tickerSymbol) {

        return stockServicePort.findByTickerSymbol(tickerSymbol)
            .map(ResponseEntity::ok)
            .orElse(ResponseEntity.notFound().build());
    }

}
