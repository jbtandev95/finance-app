package com.jbtan.financeapp.adapter.jpa.stock.mapper;

import com.jbtan.financeapp.adapter.jpa.stock.entity.StockJpaEntity;
import com.jbtan.financeapp.domain.domain.stock.Stock;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface StockJpaMapper {

    Stock toDomain(StockJpaEntity stockJpaEntity);
}
