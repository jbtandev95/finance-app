package com.jbtan.financeapp.adapter.jpa.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.data.repository.query.SecurityEvaluationContextExtension;

@Configuration
@EnableJpaRepositories(basePackages = "com.jbtan.financeapp.adapter")
@EntityScan("com.jbtan.financeapp.adapter.jpa")
@EnableJpaAuditing
public class JpaConfig {

    @Bean
    public SecurityEvaluationContextExtension securityEvaluationContextExtension(){
        return new SecurityEvaluationContextExtension();
    }

}
