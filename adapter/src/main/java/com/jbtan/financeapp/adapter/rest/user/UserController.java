package com.jbtan.financeapp.adapter.rest.user;

import com.jbtan.financeapp.adapter.rest.dto.LoginUserWebDTO;
import com.jbtan.financeapp.adapter.rest.dto.RegisterUserWebDTO;
import com.jbtan.financeapp.adapter.rest.mapper.UserWebMapper;
import com.jbtan.financeapp.domain.domain.user.User;
import com.jbtan.financeapp.domain.dto.UserDTO;
import com.jbtan.financeapp.domain.port.in.user.UserServicePort;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1")
@Slf4j
@RequiredArgsConstructor
@Tag(name = "Users", description = "User Operations")
public class UserController {

    public static final String AUTHORIZATION_HEADER = "Authorization";

    private final UserServicePort userServicePort;

    private final UserWebMapper userWebMapper;

    @PostMapping("/users")
    public ResponseEntity<User> registerUser(@RequestBody @Valid RegisterUserWebDTO userDTO) {
        return ResponseEntity.ok(userServicePort.createUser(userWebMapper.toDomainDto(userDTO)));
    }

    @PostMapping("/users/authenticate")
    public ResponseEntity<?> authenticate(@RequestBody @Valid LoginUserWebDTO loginUserWebDTO) {
        UserDTO userDTO = userWebMapper.toDomainDto(loginUserWebDTO);

        userServicePort.authenticate(userDTO);
        String token = userServicePort.getToken(userDTO);
        HttpHeaders headers = new HttpHeaders();
        headers.add(AUTHORIZATION_HEADER, "Bearer " + token);

        return ResponseEntity.ok().headers(headers).body(token);
    }
}
