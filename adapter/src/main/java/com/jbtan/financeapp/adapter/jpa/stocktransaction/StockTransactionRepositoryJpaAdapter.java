package com.jbtan.financeapp.adapter.jpa.stocktransaction;

import com.jbtan.financeapp.adapter.jpa.stocktransaction.entity.StockTransactionJpaEntity;
import com.jbtan.financeapp.adapter.jpa.stocktransaction.mapper.StockTransactionJpaMapper;
import com.jbtan.financeapp.adapter.jpa.stocktransaction.repository.StockTransactionJpaRepository;
import com.jbtan.financeapp.common.annotations.Adapters;
import com.jbtan.financeapp.domain.domain.stocktransaction.StockTransaction;
import com.jbtan.financeapp.domain.port.out.stocktransaction.StockTransactionRepositoryPort;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Adapters
@RequiredArgsConstructor
@Component
@Transactional(readOnly = true)
@Slf4j
public class StockTransactionRepositoryJpaAdapter implements StockTransactionRepositoryPort {

    private final StockTransactionJpaRepository stockTransactionJpaRepository;

    private final StockTransactionJpaMapper stockTransactionJpaMapper;

    @Override
    @Transactional(readOnly = false)
    public StockTransaction save(StockTransaction stockTransaction) {
        // get current user
        return stockTransactionJpaMapper.toDomain(stockTransactionJpaRepository.save(stockTransactionJpaMapper.toEntity(stockTransaction)));
    }

    @Override
    public Optional<StockTransaction> findByIdAndLoggedInUser(Long id) {
        return stockTransactionJpaRepository.findByIdAndLoggedInUser(id)
            .map(stockTransactionJpaMapper::toDomain);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Long id) {
        stockTransactionJpaRepository.delete(id);
    }

    @Override
    public List<StockTransaction> getAllByLoggedInUser() {
        return stockTransactionJpaRepository.getAllByLoggedInUser().stream().map(stockTransactionJpaMapper::toDomainLazyFetch).collect(Collectors.toList());
    }
}
