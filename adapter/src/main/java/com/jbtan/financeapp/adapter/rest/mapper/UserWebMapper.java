package com.jbtan.financeapp.adapter.rest.mapper;

import com.jbtan.financeapp.adapter.rest.dto.LoginUserWebDTO;
import com.jbtan.financeapp.adapter.rest.dto.RegisterUserWebDTO;
import com.jbtan.financeapp.domain.domain.user.User;
import com.jbtan.financeapp.domain.dto.UserDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserWebMapper {

    User toDomain(RegisterUserWebDTO webDTO);

    UserDTO toDomainDto(RegisterUserWebDTO webDTO);

    UserDTO toDomainDto(LoginUserWebDTO loginUserWebDTO);
}
