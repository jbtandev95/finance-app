package com.jbtan.financeapp.adapter.jpa.stocktransaction.entity;

import com.jbtan.financeapp.adapter.common.BaseJpaEntity;
import com.jbtan.financeapp.adapter.jpa.stock.entity.StockJpaEntity;
import com.jbtan.financeapp.adapter.jpa.user.entity.UserJpaEntity;
import com.jbtan.financeapp.domain.domain.stock.Stock;
import com.jbtan.financeapp.domain.enumeration.common.CurrencyCode;
import com.jbtan.financeapp.domain.enumeration.stocktransaction.StockTransactionStatus;
import com.jbtan.financeapp.domain.enumeration.stocktransaction.StockTransactionType;
import jakarta.persistence.*;
import lombok.Data;
import org.springframework.data.annotation.CreatedBy;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Entity
@Data
@Table(name = "stock_transaction")
public class StockTransactionJpaEntity extends BaseJpaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "amount", nullable = false)
    private BigDecimal amount;

    @Column(name = "transaction_date", nullable = false)
    private ZonedDateTime transactionDate;

    @Column(name = "currency", nullable = false)
    @Enumerated(EnumType.STRING)
    private CurrencyCode currencyCode;

    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private StockTransactionStatus status;

    @Column(name = "type", nullable = false)
    @Enumerated(EnumType.STRING)
    private StockTransactionType type;

    @ManyToOne(fetch = FetchType.LAZY)
    private StockJpaEntity stock;

    @ManyToOne(fetch = FetchType.LAZY)
    private UserJpaEntity user;

}
