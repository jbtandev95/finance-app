package com.jbtan.financeapp.adapter.rest.stocktransaction;

import com.jbtan.financeapp.adapter.rest.dto.PatchStockTransactionWebDTO;
import com.jbtan.financeapp.adapter.rest.dto.StockTransactionWebDTO;
import com.jbtan.financeapp.adapter.rest.exception.BadRequestException;
import com.jbtan.financeapp.adapter.rest.mapper.StockTransactionWebMapper;
import com.jbtan.financeapp.domain.port.in.stocktransaction.StockTransactionServicePort;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@RestController
@RequestMapping("/api/v1/stock-transactions")
@Slf4j
@RequiredArgsConstructor
public class StockTransactionController {

    private final StockTransactionServicePort stockTransactionServicePort;

    private final StockTransactionWebMapper stockTransactionWebMapper;

    @GetMapping
    public ResponseEntity<?> getAllByLoggedInUser() {
        return ResponseEntity.ok(stockTransactionServicePort.getAllByLoggedInUser());
    }

    @PostMapping
    public ResponseEntity<?> createNewTransaction(@Valid @RequestBody StockTransactionWebDTO webDTO) {
        return ResponseEntity.ok(stockTransactionServicePort.save(stockTransactionWebMapper.toDomain(webDTO)));
    }

    @PatchMapping(value = "/{id}", consumes = { "application/json"})
    public ResponseEntity<?> updateTransaction(@PathVariable final Long id, @NotNull @RequestBody PatchStockTransactionWebDTO webDTO) {
        if (webDTO.id() == null) {
            throw new BadRequestException("No ID Requested");
        }

        if (!Objects.equals(id, webDTO.id())) {
            throw new BadRequestException("Invalid ID");
        }

        return stockTransactionServicePort.partialUpdate(stockTransactionWebMapper.toDomain(webDTO))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteTransaction(@PathVariable final Long id) {

        stockTransactionServicePort.delete(id);

        return ResponseEntity.noContent().build();
    }
}
