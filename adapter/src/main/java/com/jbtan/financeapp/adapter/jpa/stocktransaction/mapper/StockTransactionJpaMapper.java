package com.jbtan.financeapp.adapter.jpa.stocktransaction.mapper;

import com.jbtan.financeapp.adapter.jpa.stock.entity.StockJpaEntity;
import com.jbtan.financeapp.adapter.jpa.stocktransaction.entity.StockTransactionJpaEntity;
import com.jbtan.financeapp.domain.domain.stock.Stock;
import com.jbtan.financeapp.domain.domain.stocktransaction.StockTransaction;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface StockTransactionJpaMapper {

    StockTransaction toDomain(StockTransactionJpaEntity stockTransactionJpaEntity);

    @Mapping(source = "stock", target = "stock")
    @Mapping(source = "user", target = "user")
    StockTransactionJpaEntity toEntity(StockTransaction stockTransaction);

    @Mapping(target = "stock", source = "stock", ignore = true)
    @Mapping(target = "user", source = "user", ignore = true)
    StockTransaction toDomainLazyFetch(StockTransactionJpaEntity stockTransactionJpaEntity);
}
