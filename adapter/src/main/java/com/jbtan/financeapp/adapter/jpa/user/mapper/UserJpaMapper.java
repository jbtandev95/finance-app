package com.jbtan.financeapp.adapter.jpa.user.mapper;

import com.jbtan.financeapp.adapter.jpa.user.entity.UserJpaEntity;
import com.jbtan.financeapp.domain.domain.user.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserJpaMapper {

    User toDomain(UserJpaEntity userJpaEntity);

    UserJpaEntity toEntity(User user);
}
