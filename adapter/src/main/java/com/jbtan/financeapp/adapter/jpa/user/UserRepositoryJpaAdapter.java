package com.jbtan.financeapp.adapter.jpa.user;

import com.jbtan.financeapp.adapter.jpa.user.entity.UserJpaEntity;
import com.jbtan.financeapp.adapter.jpa.user.mapper.UserJpaMapper;
import com.jbtan.financeapp.adapter.jpa.user.repository.UserJpaRepository;
import com.jbtan.financeapp.common.annotations.Adapters;
import com.jbtan.financeapp.domain.domain.user.User;
import com.jbtan.financeapp.domain.port.out.user.UserRepositoryPort;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Component
@Adapters
@RequiredArgsConstructor
@Slf4j
public class UserRepositoryJpaAdapter implements UserRepositoryPort {

    private final UserJpaRepository userJpaRepository;

    private final UserJpaMapper userJpaMapper;

    @Override
    public boolean findIfExistingLoginExists(String login) {
        return userJpaRepository.findIfExistingLoginExists(login).isPresent();
    }

    @Override
    public boolean findIfExistingEmailExists(String email) {
        return userJpaRepository.findIfExistingEmailExists(email).isPresent();
    }

    @Override
    public User saveUser(User user) {
        return userJpaMapper.toDomain(userJpaRepository.save(userJpaMapper.toEntity(user)));
    }

    @Override
    public Optional<User> findUserByLogin(String login) {
        return userJpaRepository.findUserByLogin(login)
            .map(userJpaMapper::toDomain);
    }

    @Override
    public Optional<Long> findUserIdByLogin(String login) {
        return userJpaRepository.findUserByLogin(login).map(UserJpaEntity::getId);
    }
}
