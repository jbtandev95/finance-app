package com.jbtan.financeapp.adapter.rest.dto;

import jakarta.validation.constraints.NotNull;

public record RegisterUserWebDTO(@NotNull String login, @NotNull String password, @NotNull String email, @NotNull String firstName, @NotNull String lastName) {
}
