package com.jbtan.financeapp.adapter.rest.dto;

import com.jbtan.financeapp.domain.enumeration.common.CurrencyCode;
import com.jbtan.financeapp.domain.enumeration.stocktransaction.StockTransactionStatus;
import com.jbtan.financeapp.domain.enumeration.stocktransaction.StockTransactionType;
import jakarta.validation.constraints.NotNull;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

public record StockTransactionWebDTO(@NotNull BigDecimal amount, @NotNull Long stockId,
                                     @NotNull StockTransactionStatus status, @NotNull CurrencyCode currencyCode,
                                     @NotNull ZonedDateTime transactionDate, @NotNull StockTransactionType type) {
}
