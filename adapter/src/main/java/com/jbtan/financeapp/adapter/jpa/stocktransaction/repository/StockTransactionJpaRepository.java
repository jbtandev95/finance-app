package com.jbtan.financeapp.adapter.jpa.stocktransaction.repository;

import com.jbtan.financeapp.adapter.jpa.stocktransaction.entity.StockTransactionJpaEntity;
import com.jbtan.financeapp.domain.domain.stocktransaction.StockTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StockTransactionJpaRepository extends JpaRepository<StockTransactionJpaEntity, Long> {

    @Query("select st " +
        "from StockTransactionJpaEntity st " +
        "where st.id = :id " +
        "and st.user.login = ?#{principal.username} ")
    Optional<StockTransactionJpaEntity> findByIdAndLoggedInUser(@Param("id") Long id );

    @Modifying
    @Query(value = "update StockTransactionJpaEntity st " +
        "set st.status = 'DELETED' " +
        "where st.id = :id " +
            "and st.user.login = ?#{principal.username} ")
    int delete(@Param("id") Long id);

    @Query("select st " +
        "from StockTransactionJpaEntity st " +
        "where st.user.login = ?#{principal.username} ")
    List<StockTransactionJpaEntity> getAllByLoggedInUser();
}
