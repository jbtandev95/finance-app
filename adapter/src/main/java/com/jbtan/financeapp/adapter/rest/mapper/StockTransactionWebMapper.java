package com.jbtan.financeapp.adapter.rest.mapper;

import com.jbtan.financeapp.adapter.rest.dto.PatchStockTransactionWebDTO;
import com.jbtan.financeapp.adapter.rest.dto.StockTransactionWebDTO;
import com.jbtan.financeapp.domain.domain.stocktransaction.StockTransaction;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface StockTransactionWebMapper {


    StockTransaction toDomain(StockTransactionWebDTO stockTransactionWebDTO);

    StockTransaction toDomain(PatchStockTransactionWebDTO patchStockTransactionWebDTO);
}
