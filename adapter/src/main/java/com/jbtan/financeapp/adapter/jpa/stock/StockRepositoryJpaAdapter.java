package com.jbtan.financeapp.adapter.jpa.stock;

import com.jbtan.financeapp.adapter.jpa.stock.entity.StockJpaEntity;
import com.jbtan.financeapp.adapter.jpa.stock.mapper.StockJpaMapper;
import com.jbtan.financeapp.common.annotations.Adapters;
import com.jbtan.financeapp.domain.domain.stock.Stock;
import com.jbtan.financeapp.domain.port.out.stock.StockRepositoryPort;
import com.jbtan.financeapp.adapter.jpa.stock.repository.StockJpaRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Adapters
@Component
@Slf4j
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class StockRepositoryJpaAdapter implements StockRepositoryPort {

    private final StockJpaRepository stockJpaRepository;

    private final StockJpaMapper stockJpaMapper;

//    @Cacheable(key = "'stock_'+ #tickerSymbol", value = "stock")
    public Optional<Stock> findByTickerSymbol(String tickerSymbol) {
        log.info("getting stock {} from db", tickerSymbol);
        return stockJpaRepository.findByTickerSymbol(tickerSymbol)
            .map(stockJpaMapper::toDomain);
    }
}
