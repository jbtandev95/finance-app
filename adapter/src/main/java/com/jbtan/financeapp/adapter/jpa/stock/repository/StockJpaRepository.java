package com.jbtan.financeapp.adapter.jpa.stock.repository;

import com.jbtan.financeapp.adapter.jpa.stock.entity.StockJpaEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StockJpaRepository extends JpaRepository<StockJpaEntity, Long> {

    @Query(value = "SELECT jpa FROM StockJpaEntity jpa " +
        "WHERE jpa.tickerSymbol = :tickerSymbol ")
    Optional<StockJpaEntity> findByTickerSymbol(@Param("tickerSymbol") String tickerSymbol);
}
