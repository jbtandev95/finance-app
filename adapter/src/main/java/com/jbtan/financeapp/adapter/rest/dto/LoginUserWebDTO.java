package com.jbtan.financeapp.adapter.rest.dto;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;

public record LoginUserWebDTO(@NotNull String login, @NotNull String password) {
}
