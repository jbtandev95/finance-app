package com.jbtan.financeapp.adapter.jpa.stock.entity;

import com.jbtan.financeapp.adapter.common.BaseJpaEntity;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cache;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "stock")
@Data
@Cache(region = "stockCache", usage = CacheConcurrencyStrategy.READ_ONLY)
public class StockJpaEntity extends BaseJpaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "ticker_symbol")
    private String tickerSymbol;

    @Column(name = "code")
    private String code;
}
