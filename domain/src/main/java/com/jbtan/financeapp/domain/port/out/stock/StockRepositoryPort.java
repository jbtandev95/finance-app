package com.jbtan.financeapp.domain.port.out.stock;

import com.jbtan.financeapp.common.annotations.Ports;
import com.jbtan.financeapp.domain.domain.stock.Stock;

import java.util.Optional;

@Ports
public interface StockRepositoryPort {

    Optional<Stock> findByTickerSymbol(String tickerSymbol);
}
