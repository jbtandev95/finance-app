package com.jbtan.financeapp.domain.port.out.stocktransaction;

import com.jbtan.financeapp.domain.domain.stocktransaction.StockTransaction;

import java.util.List;
import java.util.Optional;

public interface StockTransactionRepositoryPort {

    StockTransaction save(StockTransaction stockTransaction);

    Optional<StockTransaction> findByIdAndLoggedInUser(Long id);

    void delete(Long id);

    List<StockTransaction> getAllByLoggedInUser();
}
