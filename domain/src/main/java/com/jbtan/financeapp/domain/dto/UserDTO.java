package com.jbtan.financeapp.domain.dto;

public record UserDTO(Long id, String login, String password, String email, String firstName, String lastName, boolean activated) {
}
