package com.jbtan.financeapp.domain.domain.user;

import com.jbtan.financeapp.common.annotations.Domains;
import com.jbtan.financeapp.common.domain.BaseDomain;
import com.jbtan.financeapp.domain.dto.UserDTO;
import com.jbtan.financeapp.domain.exception.CreateUserException;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@Builder
@Domains
public class User extends BaseDomain {

    private Long id;

    private String login;

    private String password;

    private String firstName;

    private String lastName;

    private String email;

    private boolean activated;

    public void createUser(UserDTO userDTO) throws CreateUserException {

        if (userDTO.id() != null) {
            throw new CreateUserException();
        }
    }
}
