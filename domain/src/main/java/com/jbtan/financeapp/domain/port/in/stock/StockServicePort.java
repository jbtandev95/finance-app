package com.jbtan.financeapp.domain.port.in.stock;

import com.jbtan.financeapp.common.annotations.Ports;
import com.jbtan.financeapp.domain.domain.stock.Stock;

import java.util.Optional;

@Ports
public interface StockServicePort {
    Optional<Stock> findByTickerSymbol(String tickerSymbol);
}
