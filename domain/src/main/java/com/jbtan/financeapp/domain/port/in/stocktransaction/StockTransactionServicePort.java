package com.jbtan.financeapp.domain.port.in.stocktransaction;

import com.jbtan.financeapp.common.annotations.Ports;
import com.jbtan.financeapp.domain.domain.stocktransaction.StockTransaction;

import java.util.List;
import java.util.Optional;

@Ports
public interface StockTransactionServicePort {

    StockTransaction save(StockTransaction stockTransaction);

    Optional<StockTransaction> partialUpdate(StockTransaction stockTransaction);

    void delete(Long id);

    List<StockTransaction> getAllByLoggedInUser();
}
