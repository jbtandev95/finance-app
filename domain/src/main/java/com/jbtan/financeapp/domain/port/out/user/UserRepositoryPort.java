package com.jbtan.financeapp.domain.port.out.user;

import com.jbtan.financeapp.domain.domain.user.User;

import java.util.Optional;

public interface UserRepositoryPort {

    boolean findIfExistingLoginExists(String login);

    boolean findIfExistingEmailExists(String email);

    User saveUser(User user);

    Optional<User> findUserByLogin(String login);

    Optional<Long> findUserIdByLogin(String login);
}
