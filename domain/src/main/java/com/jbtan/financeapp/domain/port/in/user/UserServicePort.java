package com.jbtan.financeapp.domain.port.in.user;

import com.jbtan.financeapp.common.annotations.Ports;
import com.jbtan.financeapp.domain.dto.UserDTO;
import com.jbtan.financeapp.domain.domain.user.User;

@Ports
public interface UserServicePort {

    User createUser(UserDTO userDTO);

    boolean findIfExistingLoginExists(String login);

    boolean findIfExistingEmailExists(String email);

    void authenticate(UserDTO userDTO);

    boolean logout(UserDTO userDTO);

    String getToken(UserDTO userDTO);
}
