package com.jbtan.financeapp.domain.enumeration.stocktransaction;

public enum StockTransactionType {

    BOUGHT, SOLD
}
