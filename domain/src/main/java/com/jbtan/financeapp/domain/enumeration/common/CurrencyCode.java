package com.jbtan.financeapp.domain.enumeration.common;

public enum CurrencyCode {

    MYR, THB, IDR, SGD, USD;
}
