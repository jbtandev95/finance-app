package com.jbtan.financeapp.domain.enumeration.stocktransaction;

public enum StockTransactionStatus {

    COMPLETED, PENDING, DELETED;
}
