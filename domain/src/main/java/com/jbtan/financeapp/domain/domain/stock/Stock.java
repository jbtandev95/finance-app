package com.jbtan.financeapp.domain.domain.stock;

import com.jbtan.financeapp.common.annotations.Domains;
import com.jbtan.financeapp.common.domain.BaseDomain;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@Domains
public class Stock extends BaseDomain {

    private Long id;
    private String name;
    private String tickerSymbol;
    private String code;
}
