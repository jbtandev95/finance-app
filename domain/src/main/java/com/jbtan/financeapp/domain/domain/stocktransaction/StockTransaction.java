package com.jbtan.financeapp.domain.domain.stocktransaction;

import com.jbtan.financeapp.common.annotations.Domains;
import com.jbtan.financeapp.common.domain.BaseDomain;
import com.jbtan.financeapp.domain.enumeration.common.CurrencyCode;
import com.jbtan.financeapp.domain.domain.stock.Stock;
import com.jbtan.financeapp.domain.domain.user.User;
import com.jbtan.financeapp.domain.enumeration.stocktransaction.StockTransactionStatus;
import com.jbtan.financeapp.domain.enumeration.stocktransaction.StockTransactionType;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Data
@Domains
@EqualsAndHashCode(callSuper = true)
public class StockTransaction extends BaseDomain {

    private Long id;

    private BigDecimal amount;

    private Stock stock;

    private User user;

    private StockTransactionStatus status;

    private StockTransactionType type;

    private CurrencyCode currencyCode;

    private ZonedDateTime transactionDate;

}
