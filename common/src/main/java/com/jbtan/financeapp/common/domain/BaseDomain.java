package com.jbtan.financeapp.common.domain;

import lombok.Data;

import java.io.Serializable;
import java.time.Instant;

@Data
public abstract class BaseDomain implements Serializable {

    private Instant createdDate;

    private String createdBy;

    private Instant lastModifiedDate;

    private String lastModifiedBy;

}
