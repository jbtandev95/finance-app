package com.jbtan.financeapp.application.service.stocktransaction;

import com.jbtan.financeapp.application.security.token.CustomUserDetails;
import com.jbtan.financeapp.application.security.util.SecurityUtils;
import com.jbtan.financeapp.application.service.stocktransaction.mapper.StockTransactionMapper;
import com.jbtan.financeapp.common.annotations.UseCases;
import com.jbtan.financeapp.domain.domain.stocktransaction.StockTransaction;
import com.jbtan.financeapp.domain.domain.user.User;
import com.jbtan.financeapp.domain.port.in.stocktransaction.StockTransactionServicePort;
import com.jbtan.financeapp.domain.port.out.stocktransaction.StockTransactionRepositoryPort;
import com.jbtan.financeapp.domain.port.out.user.UserRepositoryPort;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
@UseCases
public class StockTransactionApplicationService implements StockTransactionServicePort {

    private final StockTransactionRepositoryPort stockTransactionRepositoryPort;

    private final UserRepositoryPort userRepositoryPort;

    private final StockTransactionMapper stockTransactionMapper;


    @Override
    public StockTransaction save(StockTransaction stockTransaction) {

        CustomUserDetails loggedInUser = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Long userId = userRepositoryPort.findUserIdByLogin(loggedInUser.getUsername()).orElseThrow();

        stockTransaction.setUser(User.builder().id(userId).build());

        return stockTransactionRepositoryPort.save(stockTransaction);
    }

    @Override
    public Optional<StockTransaction> partialUpdate(StockTransaction stockTransaction) {
        return stockTransactionRepositoryPort.findByIdAndLoggedInUser(stockTransaction.getId()).map(existing -> {
            stockTransactionMapper.partialUpdate(existing, stockTransaction);
            return existing;
        }).map(stockTransactionRepositoryPort::save);
    }

    @Override
    public void delete(Long id) {
        stockTransactionRepositoryPort.delete(id);
    }

    @Override
    public List<StockTransaction> getAllByLoggedInUser() {
        return stockTransactionRepositoryPort.getAllByLoggedInUser();
    }
}
