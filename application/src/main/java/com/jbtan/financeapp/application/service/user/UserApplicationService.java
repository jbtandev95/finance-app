package com.jbtan.financeapp.application.service.user;

import com.jbtan.financeapp.application.security.jwt.JWTClaimsKey;
import com.jbtan.financeapp.common.annotations.UseCases;
import com.jbtan.financeapp.domain.domain.user.User;
import com.jbtan.financeapp.domain.dto.UserDTO;
import com.jbtan.financeapp.application.security.token.TokenProviderPort;
import com.jbtan.financeapp.domain.port.in.user.UserServicePort;
import com.jbtan.financeapp.domain.port.out.user.UserRepositoryPort;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
@UseCases
public class UserApplicationService implements UserServicePort {

    private final UserRepositoryPort userRepositoryPort;

    private final PasswordEncoder passwordEncoder;

    private final TokenProviderPort tokenProviderPort;

    private final AuthenticationManager authenticationManager;

    private static final String CLAIMS_LOGIN_KEY = "login";

    @Override
    @Transactional(rollbackFor = Exception.class)
    public User createUser(UserDTO userDTO) {

        if (userDTO.id() != null) {
            throw new RuntimeException("User Already Exists");
        }

        if (userRepositoryPort.findIfExistingLoginExists(userDTO.login().toLowerCase())) {
            throw new RuntimeException("User Already Exists");
        }

        if (userRepositoryPort.findIfExistingEmailExists(userDTO.email().toLowerCase())) {
            throw new RuntimeException("User Already Exists");
        }

        User user = User.builder()
            .login(userDTO.login().toLowerCase())
            .email(StringUtils.isNotBlank(userDTO.email()) ? userDTO.email().toLowerCase() : null)
            .activated(false)
            .firstName(userDTO.firstName())
            .lastName(userDTO.lastName())
            .password(passwordEncoder.encode(userDTO.password()))
            .build();

        return userRepositoryPort.saveUser(user);
    }

    @Override
    public boolean findIfExistingLoginExists(String login) {
        return userRepositoryPort.findIfExistingLoginExists(login.toLowerCase());
    }

    @Override
    public boolean findIfExistingEmailExists(String email) {
        return userRepositoryPort.findIfExistingEmailExists(email.toLowerCase());
    }

    @Override
    public void authenticate(UserDTO userDTO) {
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userDTO.login(),
            userDTO.password());

        Authentication authentication = authenticationManager.authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @Override
    public boolean logout(UserDTO userDTO) {
        return false;
    }

    @Override
    public String getToken(UserDTO userDTO) {
        Map<String, String> claims = new HashMap<>();
        claims.put(JWTClaimsKey.login.name(), userDTO.login());
        return tokenProviderPort.createToken("", "", claims, 500L);
    }
}
