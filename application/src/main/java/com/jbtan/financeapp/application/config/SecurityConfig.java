package com.jbtan.financeapp.application.config;

import com.jbtan.financeapp.application.security.jwt.JWTConfigurer;
import com.jbtan.financeapp.application.security.token.TokenProviderPort;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.annotation.web.configurers.LogoutConfigurer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
@Slf4j
@RequiredArgsConstructor
public class SecurityConfig  {

    private final TokenProviderPort tokenProviderPort;

    private static final String[] ENDPOINT_WHITELIST = {
        "/home",
        "/api/v1/users/authenticate",
//        "/api/v1/**",
        "/swagger-ui.html",
        "/v2/api-docs",
        "/v3/api-docs",
        "/webjars/**",
        "/actuator/**"
    };

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
            .authorizeHttpRequests((requests) -> requests
                .requestMatchers(ENDPOINT_WHITELIST).permitAll()
                .anyRequest().authenticated()
            )
            .csrf(AbstractHttpConfigurer::disable) //
            .logout(LogoutConfigurer::permitAll)
            .apply(getConfigurer());

        return http.build();
    }

    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    private JWTConfigurer getConfigurer() {
        return new JWTConfigurer(tokenProviderPort);
    }
}
