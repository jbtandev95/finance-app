package com.jbtan.financeapp.application.security.jwt;

import com.jbtan.financeapp.application.security.token.CustomUserDetails;
import com.jbtan.financeapp.application.security.token.TokenProviderPort;
import com.jbtan.financeapp.common.annotations.Adapters;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;

@Adapters
@Component
@Slf4j
public class TokenJWTAdapter implements TokenProviderPort {

    private static final String AUTHORITIES_KEY = "auth";

    private static final String INVALID_JWT_TOINVALID_JWT_TOKENKEN = "Invalid JWT token.";

    private final SecretKey key;

    private final JwtParser jwtParsers;

    public TokenJWTAdapter(@Value("application.security.base64-secret-key") String secretKey) {
        log.info("secretKey {}", secretKey);
        this.key = Keys.hmacShaKeyFor(Decoders.BASE64.decode("ZGU0YjcxOWE3OWYyZTFiMmUwMzk5MjEzYWUxYmQzNDJjY2I3OGExMjQxYzNjMmVkNjAyNmU0MjhkMTkwMDhjZjQ0MWIzYjVkZWFlMmZjODcwMjM0YmU3OWExNDM0ZjNjN2Q4NTQxY2U1NzIyYTkzYzQzMGJhYjUxNDU3MDUzMTk"));
        this.jwtParsers = Jwts.parser().verifyWith(this.key).build();
    }
    @Override
    public String createToken(String subject, String authorities, Map<String, String> claims, Long minutes) {
        Date issuedDate = new Date(System.currentTimeMillis());
        Date expiry = new Date(System.currentTimeMillis() + (minutes * 1000 * 30));

        return Jwts.builder()
            .claims(claims)
            .subject(subject)
            .issuedAt(issuedDate)
            .expiration(expiry)
            .signWith(key)
            .compact();
    }

    @Override
    public boolean validateToken(String token) {
        this.jwtParsers.parseSignedClaims(token);
        return true;
    }

    @Override
    public Authentication getAuthentication(String token) {
        Claims claims = jwtParsers.parseSignedClaims(token).getBody();

        String login = (String) claims.get(JWTClaimsKey.login.name());

        CustomUserDetails principal = new CustomUserDetails(login, "", new HashSet<>());

        return new UsernamePasswordAuthenticationToken(principal, token, null);
    }
}
