package com.jbtan.financeapp.application.security.jwt;

import com.jbtan.financeapp.domain.domain.user.User;
import com.jbtan.financeapp.domain.port.out.user.UserRepositoryPort;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
@Slf4j
public class JWTAuthenticationManager implements AuthenticationManager {

    private final PasswordEncoder passwordEncoder;

    private final UserRepositoryPort userRepositoryPort;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        Optional<User> userOpt = userRepositoryPort.findUserByLogin(authentication.getPrincipal().toString());

        if (userOpt.isPresent()) {
            User user = userOpt.get();
            if (passwordEncoder.matches(authentication.getCredentials().toString(), user.getPassword())) {
                return new UsernamePasswordAuthenticationToken(authentication.getPrincipal(), authentication.getCredentials());
            } else {
                throw new BadCredentialsException("Wrong Username or Password");
            }
        }

        throw new BadCredentialsException("Wrong Username or Password");
    }
}
