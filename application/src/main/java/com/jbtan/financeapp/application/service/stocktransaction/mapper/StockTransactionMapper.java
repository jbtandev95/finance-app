package com.jbtan.financeapp.application.service.stocktransaction.mapper;

import com.jbtan.financeapp.domain.domain.stocktransaction.StockTransaction;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public interface StockTransactionMapper {

    @Named("partialUpdate")
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void partialUpdate(@MappingTarget StockTransaction domain, StockTransaction dto);
}
