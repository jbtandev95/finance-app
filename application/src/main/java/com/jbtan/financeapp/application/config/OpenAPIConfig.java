package com.jbtan.financeapp.application.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@OpenAPIDefinition(
    info = @Info(
        description = "OpenAPI documentation for Finance App",
        title = "OpenAPI specification - Finance App",
        version = "1.0"
    )
)
@SecurityScheme(
    name = "bearerAuth",
    description = "JWT Auth Description",
    scheme = "bearer",
    type = SecuritySchemeType.HTTP,
    bearerFormat = "JWT",
    in = SecuritySchemeIn.HEADER
)
@Profile("dev")
@Configuration
public class OpenAPIConfig {

}
