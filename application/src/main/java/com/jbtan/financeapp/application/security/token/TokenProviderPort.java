package com.jbtan.financeapp.application.security.token;

import com.jbtan.financeapp.common.annotations.Ports;
import org.springframework.security.core.Authentication;

import java.util.Map;

@Ports
public interface TokenProviderPort {

    String createToken(String subject, String authorities, Map<String, String> claims, Long minutes);

    boolean validateToken(String token);

    Authentication getAuthentication(String token);
}
