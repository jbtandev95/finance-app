package com.jbtan.financeapp.application.service.stock;

import com.jbtan.financeapp.common.annotations.UseCases;
import com.jbtan.financeapp.domain.domain.stock.Stock;
import com.jbtan.financeapp.domain.port.in.stock.StockServicePort;
import com.jbtan.financeapp.domain.port.out.stock.StockRepositoryPort;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
@UseCases
public class StockApplicationService implements StockServicePort {

    private final StockRepositoryPort stockRepositoryPort;

    @Override
    public Optional<Stock> findByTickerSymbol(String tickerSymbol) {
        return stockRepositoryPort.findByTickerSymbol(tickerSymbol);
    }
}
